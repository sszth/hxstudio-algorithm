#include "HXStudioAlgorithmWnd.h"


const QString g_strPluginSettingXml = "F:/hxstudio-algorithm/HXStudioAlgorithmWnd/x64/Debug/HXPlugin.xml";

HXStudioAlgorithmWnd::HXStudioAlgorithmWnd(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    _pPluginManage = QSharedPointer<CHXPluginManage>(new CHXPluginManage());
    _pPluginManage->Initialize(g_strPluginSettingXml);
    _pPluginManage->Clear();
}

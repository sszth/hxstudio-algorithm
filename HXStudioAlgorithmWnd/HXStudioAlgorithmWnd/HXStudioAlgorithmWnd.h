#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_HXStudioAlgorithmWnd.h"
#include "HXPluginManage.h"

class HXStudioAlgorithmWnd : public QMainWindow
{
    Q_OBJECT

public:
    HXStudioAlgorithmWnd(QWidget *parent = Q_NULLPTR);

private:
    Ui::HXStudioAlgorithmWndClass   ui;
    CHXPluginManagePtr               _pPluginManage;
};

#pragma once

#include "hxmultithreadprintabc_global.h"
#include "../Include/IHXPluginObject.h"

extern "C" 
HXMULTITHREADPRINTABC_EXPORT 
int HX_Initialize(int, const QString&, const QString&, IHXPluginObjectPtr&);

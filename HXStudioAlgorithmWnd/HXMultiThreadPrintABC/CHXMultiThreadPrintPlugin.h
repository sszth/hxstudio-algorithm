#pragma once
#include "..\Include\IHXPluginObject.h"

class CHXMultiThreadPrintPlugin : public IHXPluginObject
{
public:
	explicit CHXMultiThreadPrintPlugin(const int& iPluginID,
		const QString& strPluginName,
		const QString& strFileName)
		:IHXPluginObject(iPluginID, strPluginName, strFileName) {}
	explicit CHXMultiThreadPrintPlugin(const int&& iPluginID,
		const QString&& strPluginName,
		const QString&& strFileName)
		:IHXPluginObject(iPluginID, strPluginName, strFileName) {}
	virtual ~CHXMultiThreadPrintPlugin() {}
public:
	virtual int Initialize();
	virtual int Release();
};


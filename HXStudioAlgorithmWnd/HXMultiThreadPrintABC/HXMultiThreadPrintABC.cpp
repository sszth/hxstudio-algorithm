
#include "CHXMultiThreadPrintPlugin.h"
#include "HXMultiThreadPrintABC.h"

extern "C"
HXMULTITHREADPRINTABC_EXPORT
int HX_Initialize(
                    int iID, 
                    const QString & strPluginName,
                    const QString & strPluginDll,
                    IHXPluginObjectPtr & pPlugin)
{
    pPlugin = 
        QSharedPointer<IHXPluginObject>(new CHXMultiThreadPrintPlugin(iID, strPluginName, strPluginDll));
    return  0;
}

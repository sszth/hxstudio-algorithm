#include "QtWidgetsApplication1.h"
#include <qfile.h>
#include <qmessagebox.h>
#include <QtXml\qdom.h>

#pragma comment(lib,"D:\\Qt5.14.0\\6.0.0\\msvc2019_64\\lib\\Qt6Xml.lib")
//#pragma comment(lib,"D:\D:\Qt5.14.0\6.0.0\msvc2019_64\lib\Qt6Xml.lib")
QtWidgetsApplication1::QtWidgetsApplication1(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    QString fileName = QString("F:\\hxstudio - algorithm\\HXStudioAlgorithmWnd\\x64\\Debug\\HXPlugin.xml");
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::critical(this, tr("Error"),
            tr("Cannot read file %1").arg(fileName));
        //return false;
    }

    QString errorStr;
    int errorLine;
    int errorColumn;

    QDomDocument doc;
    //填充dom树
    if (!doc.setContent(&file, false, &errorStr, &errorLine,
        &errorColumn))//形参2，是否创建命名空间
    {
        QMessageBox::critical(this, tr("Error"),
            tr("Parse error at line %1, column %2: %3")
            .arg(errorLine).arg(errorColumn).arg(errorStr));
        //return false;
    }

    QDomElement root = doc.documentElement();//获取dom树的根标签
    if (root.tagName() != "bookindex")
    {
        QMessageBox::critical(this, tr("Error"),
            tr("Not a bookindex file"));
        //return false;
    }
}
